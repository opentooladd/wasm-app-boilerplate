#!/bin/env node

const process = require('process');
const path = require('path');
const fs = require('fs-extra');

let pkgName;

if (process.argv.length < 3) {
  console.error('Please provide a pkg name to link');
  process.exit(1);
} else {
  pkgName = process.argv[2];
}

const preInstall = async () => { 
  const packageJsonPath = path.resolve(__dirname, '..', 'package.json');
  const indexFilePath = path.resolve(__dirname, '..', 'src', 'index.js');
  const packageJsonFile = await fs.readJson(packageJsonPath);
  const indexFile = await fs.readFile(indexFilePath);
  const indexFileData = indexFile.toString();

  packageJsonFile.dependencies[pkgName] = 'file:../pkg';

  await fs.writeJson(packageJsonPath, packageJsonFile, {
    spaces: '\t'
  });
  await fs.writeFile(indexFilePath, indexFileData.replace(/\{\{CRATE_NAME\}\}/g, pkgName));
};

preInstall();
