import { Selector } from 'testcafe';

fixture `Init`
  .page `http://localhost:8080`;

test('Init project', async (t) => {
  await t
    .expect(Selector(() => document.querySelector('title')).innerText).eql('Hello wasm-pack!');
});
